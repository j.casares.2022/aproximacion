import sys


def compute_trees(trees): '''compute_trees = compute'''

    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production


def compute_all(min_trees, max_trees): '''=main pero en vez de print lo mete en una lista [productions]--> lisa de tuplas [(), (), ()]'''

    productions = []
    for trees in range(min_trees, max_trees +1):
        production = compute_trees(trees)

    return productions

def read_arguments():
    try:
        base_trees = int(sys.argv[1])
        fruit_per_tree = int(sys.argv[2])
        reduction = int(sys.argv[3])
        min = int(sys.argv[4])
        max = int(sys.argv[5])
    except ValueError:
        sys.exit("Tienen que ser numeros enteros")

    return base_trees, fruit_per_tree, reduction, min, max


def main():
    global base_trees
    global fruit_per_tree
    global reduction
    best_production = 0
    best_trees = 0
    base_trees, fruit_per_tree, reduction, min, max = read_arguments()

    productions = compute_all(min, max)

    for tree in productions:#bucle recorre productions y cada vez que el segundo elemeto...
        trees = tree[0]
        production = tree[1]
        print(trees, production)
        if production > best_production:
            best_trees = trees
            best_production = production

    print(f"Best production: {best_production}, for {best_trees} trees")

if __name__ == '__main__':
    main()

#sys.exit('mensaje')
#len(sys.argv) = 6
#argumentos son str pero hay que convertirlo a int -->excepcion ValueError (try...except)
#apend: meter tupla nueva en la lista